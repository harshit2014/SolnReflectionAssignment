package com.w7.soln;

import java.util.Scanner;

public class App {
	public static void main(String[] args) {
		Scanner sc = null;
		try {
			sc = new Scanner(System.in);
			int ch;
			String input, className;
			System.out.println("Please enter the class name");
			className = sc.next();
			StringBuffer sb = new StringBuffer();
			FilePersistence fp = FilePersistence.getFilePersistenceInstance();
			do {
				ReflectionGo ref;
				try {
					ref = new ReflectionGo(className);
				} catch (Exception e) {
					ref = null;
					System.out.println(
							"Unable to instantiate Class object.\nPlease provide correct and fully qualified name of the class.");
				}

				if (ref != null) {
					System.out.println("\nSelect the menu option:");
					System.out.println(
							"1. Methods\n2. Class\n3. Subclasses\n4. Parent classes\n5. Constructors\n6. Data members");
					ch = sc.nextInt();
					switch (ch) {
					case 1:
						sb.append(ref.analyseMethods());
						sb.append("\n");
						break;
					case 2:
						sb.append(ref.analyseClass());
						sb.append("\n");
						break;
					case 3:
						sb.append(ref.analyseSubClasses());
						sb.append("\n");
						break;
					case 4:
						sb.append(ref.analyseParentClasses());
						sb.append("\n");
						break;
					case 5:
						sb.append(ref.analyseConstructors());
						sb.append("\n");
						break;
					case 6:
						sb.append(ref.analyseFields());
						sb.append("\n");
						break;
					}
				}

				System.out.println(
						"\nDo you want to see any other information?\nPress Y to recheck the menu and N to continue.");
				input = sc.next();
			} while (input.equals("Y"));

			System.out.println("\nPress 1 to store collected information into file.");
			System.out.println("Press 2 to explore previous files created.");
			System.out.println("Press 3 to exit without saving.");
			ch = sc.nextInt();
			switch (ch) {
			case 1:
				fp.saveFile(className, sb.toString());
				break;
			case 2:
				System.out.println("Enter the class name to search existing documentation");
				className = sc.next();
				fp.findFile(className);
				break;
			case 3:
				break;
			}
		} catch (Exception e) {
		} finally {
			if (sc != null) {
				sc.close();
			}
		}

	}
}
