package com.w7.soln;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class ReflectionGo {

	private Class classObj;

	public ReflectionGo(String className) throws Exception {
		this.classObj = Class.forName(className);
	}

	public String analyseMethods() {
		StringBuffer sb = new StringBuffer();
		Method methods[] = classObj.getDeclaredMethods();
		sb.append("\n---METHODS (all declared ones only)----\n");
		for (Method m : methods) {
			sb.append(m);
			sb.append("\n");
		}
		methods = classObj.getMethods();
		sb.append("\n---METHODS (inherited and declared except private)----\n");
		for (Method m : methods) {
			sb.append(m);
			sb.append("\n");
		}

		System.out.println(sb.toString());
		return sb.toString();
	}

	public String analyseFields() {
		StringBuffer sb = new StringBuffer();
		Field fields[] = classObj.getDeclaredFields();
		sb.append("\n---DATA-MEMBERS (all declared ones only)----\n");
		for (Field m : fields) {
			sb.append(m);
			sb.append("\n");
		}
		fields = classObj.getFields();
		sb.append("\n---DATA-MEMBERS (inherited and declared except private)----\n");
		for (Field m : fields) {
			sb.append(m);
			sb.append("\n");
		}
		System.out.println(sb.toString());
		return sb.toString();
	}

	public String analyseClass() {
		StringBuffer sb = new StringBuffer();
		sb.append("\n----------CLASS----------\n");
		sb.append("Name of the class is : " + classObj.getName() + "\n");
		sb.append("package of the class is : " + classObj.getPackage() + "\n");
		sb.append("Access modifiers of the class are : " + Modifier.toString(classObj.getModifiers()) + "\n");
		System.out.println(sb.toString());
		return sb.toString();
	}

	public String analyseSubClasses() {
		String msg = "No functionality to determine child classes through reflection in java.";
		System.out.println(msg);
		return msg;
	}

	public String analyseParentClasses() {
		StringBuffer sb = new StringBuffer();
		sb.append("\n----------PARENT-CLASS----------\n");
		Class superClass = classObj.getSuperclass();
		sb.append("Name of the parent class is : " + superClass.getName() + "\n");
		sb.append("Access modifiers of the super-class are : " + Modifier.toString(superClass.getModifiers()) + "\n");
		System.out.println(sb.toString());
		return sb.toString();
	}

	public String analyseConstructors() {
		StringBuffer sb = new StringBuffer();
		Constructor constructors[] = classObj.getDeclaredConstructors();
		constructors = classObj.getConstructors();
		sb.append("\n--------CONSTRUCTORS---------\n");
		for (Constructor m : constructors) {
			sb.append(m);
			sb.append("\n");
		}
		System.out.println(sb.toString());
		return sb.toString();
	}

}
