package com.w7.soln;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class FilePersistence {

	public static FilePersistence instance;

	private FilePersistence() {
	}

	public static FilePersistence getFilePersistenceInstance() {
		if (instance == null) {
			instance = new FilePersistence();
		}
		return instance;
	}

	public void saveFile(String className, String info) {
		String fileName = className + ".txt";
		try {
			File f = new File(fileName);
			if (f.createNewFile()) {
				System.out.println("File created: " + f.getName());

			}
			FileWriter myWriter = new FileWriter(f);
			myWriter.write(info);
			myWriter.close();
			System.out.println("Successfully wrote to the file.");
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}
	}

	public void findFile(String className) {
		try {
			File f = new File(className + ".txt");
			Scanner myReader = new Scanner(f);
			while (myReader.hasNextLine()) {
				String data = myReader.nextLine();
				System.out.println(data);
			}
			myReader.close();
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}

	}

}
